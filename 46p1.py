class Dog:

    def __init__(self):
        self.name = "Nemesis"
        self.color = "black"
        self.age = "13"
        self.size = "small"


class MyDog(Dog):

    def __init__(self):
        super().__init__()


final_dog = MyDog()
print("My dog's name is " + final_dog.name)
print("My dog's color is " + final_dog.color)
print("My dog's age is " + final_dog.age)
print("My dog is " + final_dog.size)
