class Table:

    def first_table(self):
        print("first table")

    def second_table(self):
        print("second table")

    def __init__(self):
        print("Table init")


class Chair(Table):

    def chairs(self):
        print("Chair")


final_chair = Chair()
final_chair.chairs()
final_chair.first_table()
final_chair.second_table()
final_chair.__init__()
